package com.example.project1;

import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<Cart,Integer> {
}
