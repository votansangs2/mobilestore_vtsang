package com.example.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@Controller
public class UserControl {
    @Autowired
    private UserRepository repository;
    private UserService userService;
    @GetMapping("/register")
    public String showSignUpForm(Model model){
        model.addAttribute("user",new User());
        return "signupform";
    }

    @PostMapping("processregister")
    public String ProcessRegister(User user){
        user.setTypeaccount(2);
        repository.save(user);
        return "registersuccess";
    }

    @GetMapping("/login")
    public ModelAndView login(){
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("user",new User());
        return mav;
    }
    @PostMapping("login")
    public String login(@ModelAttribute("user") User user){
        User cauthUser = userService.login(user.getUsername(),user.getPassword());
        System.out.println(cauthUser);
        if(Objects.nonNull(cauthUser)){
            return "redirect:/";
        }else {
            return "redirect:/login";
        }
    }
}
