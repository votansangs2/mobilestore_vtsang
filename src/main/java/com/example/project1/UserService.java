package com.example.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired UserRepository repository;
    public User login(String username, String password) {
        User user = repository.findByUsernameAndPassword(username, password);
        return user;
    }
}
